import {useState} from 'react';
import {Card, Button} from "react-bootstrap";
import PropTypes from 'prop-types'; //import prop-types library
import {Link} from 'react-router-dom';


export default function CourseCard ({courseProp}){

	// console.log(props);
	// console.log(typeof props);

	const {_id, name, description, price} = courseProp;

//Syntax - const [getter, setter] = useState(initialGetterValue)
	// const [count, setCount] = useState(0); //create Dynamic variables

	// // const count = 0;

	// console.log(useState(0));

	// function enroll(){
	// 	setCount(count + 1);
	// 	console.log('Enrollees:' + count)
	// }

  const [count, setCount] = useState(0);
  const [seats, setSeats] = useState(30);

  function enroll() {

  	setCount(count + 1);
    setSeats(seats - 1);
    if (seats === 0) {
      window.alert("No more seats");
      return;
    }


    console.log(`Seats left: ${seats - 1}`);
  }	



	return (
				// <Card>
				// 	<Card.Body>
				// 		<Card.Title>{courseProp.name}</Card.Title>
				// 		<Card.Subtitle>Description</Card.Subtitle>
				// 		<Card.Text>{courseProp.description}</Card.Text>
				// 		<Card.Subtitle>Price</Card.Subtitle>
				// 		<Card.Text>{courseProp.price}</Card.Text>
				// 		<Button variant="primary">Enroll</Button>
				// 	</Card.Body>
				// </Card>
				<Card className="mt-3">
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>{price}</Card.Text>
						<Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
{/*						<Card.Text>Enrollees: {count}</Card.Text>
						<Button variant="primary" onClick={enroll}>Enroll</Button>*/}
					</Card.Body>
				</Card>

	)

}

//Check if the CourseCard component is getting the correct prop types
CourseCard.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}