import {Form, Button} from 'react-bootstrap';
import {useState, useEffect} from 'react';


export default function Login(){

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState(false);

	function userLogin(e){

		e.preventDefault();

		fetch('http://localhost:4000/users/login',{
			method: 'POST',
			headers:{
				"Content-Type":"application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})

		})
		.then(res=>res.json())
		.then(data=>{

			console.log(data)
			if (data){

				setEmail("");
				setPassword("");

				alert("Thank you for registering!")

			} else {

				alert("Please try again.")

			}

		})

	}


	useEffect(()=>{

		if(email !== "" && password !== ""){

			setIsActive(true)

		}else{

			setIsActive(false)

		}

	},[email,password])


	return (

		<Form onSubmit={(e)=>registerUser(e)}>
			<h1 className = "my-5 text-center">Register</h1>

			<Form.Group>
				<Form.Label>Email:</Form.Label>
				<Form.Control type="email" placeholder="Enter Email" required value={email} onChange={e=>{setEmail(e.target.value)}}/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password:</Form.Label>
				<Form.Control type="password" placeholder="Enter password" required value={password} onChange={e=>{setPassword(e.target.value)}}/>
			</Form.Group>

			{
				isActive ?
				<Button variant="success" type="submit" id="submitBtn">Login</Button>
				:
				<Button variant="secondary" type="submit" id="submitBtn">Login</Button>
			}

		</Form>
		

	)

}