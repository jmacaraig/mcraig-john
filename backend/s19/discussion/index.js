// Syntax, Statements and Comments

console.log("We tell the computer to log this in the console.");

alert("We tell the computer to display an alert with this message!");


// Statements are instructions the computer perform
// Ends with semicolon

// Syntax - set of rules that describes statement construction

// Comments
// use Ctrl + / - single line and multi-line comments

// console.log("Hello!");

// alert("This is an alert");
// alert("This is another alert");


// Whitespaces and linebreaks haa no impact on JS.

// Variables contain data and stored in memory
	// variable should be declared, created and ready to store data
		// Syntax
			// let or const variableName;

	let myVariable;
	console.log(myVariable); //undefined


// undefined when variable is declared but no value
// error when variable is not declared

	let hello;
	console.log(hello);

// Variable initialize
// Sample: let or const variableName = initialValue;



let productName = 'desktop computer';
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;


//re-assigning variable values

productName = 'Laptop';
console.log(productName);

// interest = 3.5; 
// console.log(interest); //cannot replace const

let friend = 'Kate';
friend = 'Jane';

// Declaration
let supplier;

// Initialization is done after the variable is declared
supplier = "John Smith Tradings";

// Re-assignment
supplier = "Zuitt Store";

// const pi; //missing initial value
const 	pi = 3.1416;
console.log(pi);


//Multiple variable declarations
let productCode = 'DC017';
const productBrand = 'Dell';
console.log(productCode,productBrand)

// let productCode='DC017', productBrand = 'Dell'

// Data Types

//Strings

let country = 'Philippines';
let province = 'Metro manila';

// Concatenating Strings

let fullAddress = province + ',' + country;
console.log(fullAddress);

let greeting = 'I live in the ' + country;
console.log(greeting);

// the escape character (\) in the strings in combination with other characters can produce different effects

// "\n" refers to creating a new line in between text

let mailAddress = 'Metro Manila\n\nPhilippines';
console.log(mailAddress);

let message = "John's employees went home early";
console.log(message);

message = 'John\'s employees went home early';
console.log(message);


//Integer or Numbers

let headcount = 26;
console.log(headcount);
let grade = 98.7;
console.log(grade);
let planetDistance = 2e10;
console.log(planetDistance);


// Combine text and strings
console.log("John's first grade is "+ grade);

// Boolean

let isMarried = false;
let isGoodConduct = true;
console.log(isMarried);

console.log("isGoodConduct: " + isGoodConduct);


// Arrays

// Collection
	// syntax: let or const arrayName = [A,B,C,D]
let grades = [98.7,92.1,90.7,98.6];
console.log(grades);

// Objects

let myGrades = {
	firstGrading: 98.7,
	secondGrading: 92.1,
	thirdGrading: 90.7,
	fourthGrading: 94.6
}
console.log(myGrades);

let person = {

	fullName: 'Juan Dela Cruz',
	age: 35,
	isMarried: false,
	contact: ["+639123456789","87000"],
	address: {
		housenumber: '345',
		city: 'Manila'
	}
}

console.log(person);

// type of person

console.log(typeof person); //object


