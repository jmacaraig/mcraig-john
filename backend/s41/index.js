//import express
const express = require("express");

//import mongoose
const mongoose = require("mongoose");

const app = express();
const port = 3001;

//[Section] Creation of todo list routes 

//Allows your app to read json data
app.use(express.json());

//Allows your app to read data from forms
app.use(express.urlencoded({exteneded:true}))


//Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://johnedisonmacaraig:9KJbQmt7MrOHlsmA@batch-297.zitwj9u.mongodb.net/taskDB?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

//variable for mongoose connection notification to db (success / error)
//Handle errors when initial connection is established
//works with on and once Mongoose methods
let db = mongoose.connection;

//console.error.bind(console) print errors in browser console and terminal
db.on("error", console.error.bind(console, "Connection error."));

//if the connection is successful output in console
db.once("open", () => console.log("We're connected to the cloud database."))

//convention nameSchema
//schema determine the structure of documents to be written in database
//schema acts as blueprint
//use the schema() constructor for mongoose module to create new schema object
//new creates new
const taskSchema = new mongoose.Schema({

	name: String,
	status: {
		type: String,
		default:"pending"
	}
});


//[Section] Models


//Models use Schemas and they act as the middleman from the server (JS code) to our database
//Serve > Schema (blueprint) > Database > Collection
//Task variable can now be used to run commands for interacting with our database
const Task = mongoose.model("Task", taskSchema)

app.post("/tasks", (req,res)=>{

	Task.findOne({
		name: req.body.name
	})
	.then((result, err)=>{

		if (result != null && result.name == req.body.name){
			return res.send("Duplicate task found.")
		} else {
			let newTask = new Task ({
				name: req.body.name
			});

			newTask.save().then((savedTask, saveErr) => {

				if (saveErr){
					return console.error(saveErr);
				} else {
					return res.status(201).send("New task created");
				}

			})
		}

	})


})


//Get all tasks

app.get("/tasks", (req,res)=>{
	//"find" is a mongoose method similar to MongoDB "find"
	Task.find({}).then((result,err)=>{
		if (err){
			return console.log(err);
		} else {
			return res.status(200).json({
				data: result
			});
		}
	})


})









if (require.main === module){

	app.listen(port,()=>console.log(`Successfully running at port ${port}.`))

}

module.exports = app;