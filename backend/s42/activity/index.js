//Setup a basic Express JS Server 

//Dependencies
const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute");

//Server Setup
const app = express();
const port = 4000;


//Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}))

//Database Connection to mongoDB Atlas
mongoose.connect("mongodb+srv://johnedisonmacaraig:9KJbQmt7MrOHlsmA@batch-297.zitwj9u.mongodb.net/taskDB?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

//console.error.bind(console) print errors in browser console and terminal
db.on("error", console.error.bind(console, "Connection error."));

//if the connection is successful output in console
db.once("open", () => console.log("We're connected to the cloud database."));

app.use("/tasks", taskRoute);












if (require.main === module){

	app.listen(port,()=>console.log(`Successfully running at port ${port}.`))

}

module.exports = app; 