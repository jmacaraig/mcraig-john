console.log('Hello World!');


//Arithmetic Operators
	//Addition, Subtraction, Multiplication, Divide, Modulo
	//+ , - , * , / , %

	let x = 1397;
	let y = 7831;

	let sum = x + y;
	console.log("Result of addition operator: "+ sum);


	//difference
	let sub = x - y;
	console.log("Difference is " +sub);

	//product
	let prod = x * y;
	console.log("Product is " + prod);

	//quotient
	let quot = x / y;
	console.log("Quotient is "+ quot);

	//remainder
	let rem = x % y;
	console.log("Remainder is "+ rem);

	let a = 10;
	let b = 5;

	let remainderA = a % b;
	console.log(remainderA); //0 if divisible

//Assignment Operator
	// Basic Assignment
	// =

let assignmentNumber = 8;

//Addition Assignment Operator

assignmentNumber = assignmentNumber + 2;

// 10+2 add and replaced
assignmentNumber+= 2;
console.log(assignmentNumber);

//Subtraction Assignment Operator

assignmentNumber -=2;
console.log('Result of -= : ' + assignmentNumber);


//Multiplication Assignment Operator

assignmentNumber *=2;
console.log('Result of *= : ' + assignmentNumber);


//Division Assignment Operator

assignmentNumber /=2;
console.log('Result of /= : ' + assignmentNumber);


//Multiple Operators and Parentheses

// 3 * 4 = 12 / 5 = 2.4 
// 1 + 2 = 3
// 3 - 2.4 = 0.6

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);


// 4 / 5 = 0.8
// 2 - 3 = -1
// 0.8 * (-1) = -0.8
// -0.8 + 1 = 0.2

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(pemdas);


//Increment and Decrement


let z = 1;

// increase first before re-assign
let increment = ++z;

console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);


// re-assign first then increase
increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " + z);


// decrease first before re-assign
let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

// re-assign first then decrease
decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z);

// Type of Coercion


let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(typeof coercion); //"1012"

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);


//true is 1
let numE = true + 1;
console.log(numE); //2


//false is 0
let numF = false + 1;
console.log(numF); //1




//Comparison Operators

let juan = 'juan';

//Equality Operator (==)

console.log(1 == 1); //true
console.log(1 == 2); //false
console.log(1 == '1'); //true
console.log(0 == false); //true
console.log('juan' == 'juan'); //true
console.log('juan' == juan); //true

//Inequality Operator (!=)

console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(1 != '1'); //false
console.log(0 != false); //false
console.log('juan' != 'juan'); //false
console.log('juan' != juan); //false


//Strict Equality Operator (==)

console.log(1 === 1); //true
console.log(1 === 2); //false
console.log(1 === '1'); //false
console.log(0 === false); //false
console.log('juan' === 'juan'); //true
console.log('juan' === juan); //true

//Strict Inequality Operator (!=)

console.log(1 !== 1); //false
console.log(1 !== 2); //true
console.log(1 !== '1'); //true
console.log(0 !== false); //true
console.log('juan' !== 'juan'); //false
console.log('juan' !== juan); //false

//Relational Operators

let abc = 50
let def = 65

let isGreaterThan = abc > def; //false
let isLessThan = abc < def; //true
let isGTOrEqual = abc >= def; //false
let isLTOrEqual = abc <= def; //true

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTOrEqual);
console.log(isLTOrEqual);

let numStr = "30";
console.log(abc > numStr); //true
console.log(def <= numStr); //false

let str = 'twenty';
console.log(def >= str); //false




//Logical Operators

	// && (AND) , || (OR) , ! (NOT)


let isLegalAge = true;
let isRegistered = false;


//&& - Return true if all operands are true

let allRequirementsMet = isLegalAge & isRegistered;
console.log("Result of AND operator: "+allRequirementsMet); //false

// || - Return true if one operand is true


let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of OR operator: "+someRequirementsMet); //true

// ! Return opposite value of reports

let someRequirementsNotMet = !isRegistered;
console.log("Result of NOT operator: "+someRequirementsNotMet); //true