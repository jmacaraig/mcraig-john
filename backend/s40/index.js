const express = require("express")
const app = express();
const port = 4000;

//Middlewares

app.use(express.json())
app.use(express.urlencoded({extended:true}))


app.get("/",(req,res)=>{

	res.send("Hello World")

})

app.get("/hello",(req,res)=>{

	res.send("Hello from hello endpoint")

})

app.post("/hello",(req,res)=>{

	res.send("Hello from post route")

})


app.post('/posts', function (req, res) {  

   res.send(`Hello ${req.body.firstName} ${req.body.lastName}` );

   })  

app.put("/",(req,res)=>{

	res.send("Hello from PUT route!")

})

app.delete("/",(req,res)=>{

	res.send("Hello from DELETE route!")

})


let users = [];


app.post('/signup',(req, res)=>{  

	if(req.body.username !== '' && req.body.password !== ''){

		users.push(req.body)
		res.send(`User ${req.body.username} has been successfully registered`)

} else {

	res.send(`Please input both username and password`)

}


   })

app.put("/change-password",(req,res)=>{
	let messagechg;

	for(let i=0; i<users.length; i++){

		if(req.body.username == users[i].username){

			users[i].password = req.body.password;

			messagechg =`Users ${req.body.username}'s password has been updated.`

			console.log(users);

			break;

		}else {

			messagechg = "User does not exist"

		}

	}

	res.send(messagechg);

})  

const port2 = 3000;

app.get("/home",(req,res)=>{
	res.send("Welcome to the home page")
})



app.get("/users",(req,res)=>{
	res.send(users)
})




app.delete("/delete-user",(req,res)=>{
	let messagedel;

if(req.body.username !== '' && req.body.password !== ''){

	for(let i=0; i<users.length; i++){

		if(req.body.username == users[i].username){

			users.splice([i],1);

			messagedel =`User ${req.body.username} deleted.`

			console.log(users);

			break;

		}else{

			messagedel = "No users found."

		}
	}

	res.send(messagedel);

	}else {

			messagedel = "User does not exist."
			res.send(messagedel);

		}

	})

	
 

app.listen(port2,()=>console.log(`Server is running at ${port2}`))


