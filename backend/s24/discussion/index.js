console.log("Javascript Loops!");

// Create a function named greeting() and display the message you want to say to yourself using console.log inside of the function
// invoke the greeting() function 5 times
//Take a screenshot

// function greeting(){
//   return console.log("Be Patient.")
// }

// greeting();
// greeting();
// greeting();
// greeting();
// greeting();

// function greeting(){
//   console.log("Keep on progressing!")
// }

// greeting();
// greeting();
// greeting();
// greeting();
// greeting();

function greeting(){
  console.log("Keep on progressing!")
}

let countNum = 50;

while (countNum !== 0){
  console.log("This is printed inside the sample loop: " + countNum);
  greeting()
  countNum--;
}

//[While loop]
//takes in an expression/condition
//expressions are any unit of code that can be evaluated to a value
//If the conditon evaluates to true, the statements inside the code block will be executed
//A loop will iterate a certain number of times untill condition is met
//Iteration - is the term given to the repetition of statements


/*

  Syntax:
  while(expression/condition){
  
  }

*/

let count = 5;

//While the value of count is not equal to 0

while (count !==0){

  //value of count is printed out
  console.log("While: " + count);
  //Decrease the value of count by 1 every iteration to stop the loop when it reaches 0
  count--;
}

//[Do while loops]
/*
    works like while loop
    guarantee that the code will be executed at least once

    syntax:
    do {
        statement
    }while (expression or condition)

*/

let number = Number(prompt("Give me a number: "))

do {
  console.log("Do While: " + number);
  //increase the value of number by 1 after every iteration to stop the loop when it reaches 10 or greater
  //number = number + 1
  number += 1;

} while (number < 10)

//[For Loop]

/*

  more flexible than while and do-while loop
  three parts:
    initialization - value that will track the progression of the loop

    expression/condition - determin whethere the loop will run one more time

    final expression - indicates how to advance the loop

  Syntax

    for(initialize, express/condition; finalexpresssion){
        statement
    }

*/

/*
    Create a lopp that will start from 0 and end at 20
    count will be check if it is equal or less than 20
    if the value is less than 20. the statement inside of the loop will execute
    The value of count will be incremented by one for each iteration
*/

  for (let count = 0; count <= 20; count++){
    console.log("For: " + count);
  }


//[Strings]

  let myString = "Taylor Swift"
  //characters in strings may be counted using the .length property
  console.log(myString.length); //output the count of characters on myString

  //accessing elements of a string
  console.log(myString[0]);
  console.log(myString[1]);
  console.log(myString[2]);

  //create a loop that will printout the individual letters of myString variable

  for (let x=0; x < myString.length; x++){
    console.log(myString[x])
  }

  //Create a string named "myName" with a value of your name

    let myName = "Cardo";

  /*
      Create a lopp that will print out the letters of the name indidividually and print out the number 3 instead of vowel letters
  */

    for (let i=0; i < myName.length; i++){

      if (
      myName[i].toLowerCase() == "a" ||
      myName[i].toLowerCase() == "e" ||
      myName[i].toLowerCase() == "i" ||
      myName[i].toLowerCase() == "o" ||
      myName[i].toLowerCase() == "u"
      ){

        console.log(3);
      }

      else {
        console.log(myName[i].toLowerCase());
      }

      //console.log(myName[i].toLowerCase());
    }


//Continue and Break Statements

/*

    -continue statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block
    -break statement is used to terminate the current loop once matche has been found

*/

/*
    create a loop that if the count value is divisible by 2 and the remainder is 0, it will print the number and continue to the next iteration of the loop
*/

for (let count = 0; count <= 20; count++){
    if (count % 2 === 0){
      continue;
    }
    console.log("Continue and Break: "+ count);
    
    if (count>10){
      break;
    }
}



/*Mini-Activity*/

let name = "Bernardo";

    for (let i=0; i < name.length; i++){

      if (name[i].toLowerCase() == "a"){
        console.log("Continue to the next iteration");
        continue;
      }
      console.log(name[i]);

      if (name[i].toLowerCase() == "d"){
        break;

      }
        
      
    }











 





