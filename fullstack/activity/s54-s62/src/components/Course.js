import {Row, Col, Card} from "react-bootstrap"
import Button from 'react-bootstrap/Button';


export default function Course (){

	return (
		<Row className="mt-5">
			<Col xs={12} md={4}>
				<Card className="courseComponent1">
					<Card.Body>
						<Card.Title>
							<h2>React JS</h2>
						</Card.Title>
						<Card.Text>
							<h5>Description:</h5>
							<p>This is a sample coures offering.</p>

							<h5>Price:</h5>
							<p>PhP 40,000</p>
						<Button variant="primary">Enroll</Button>
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>

	)

}