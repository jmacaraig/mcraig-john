console.log("Array Traversal!");

/* Mini - Activity */
// let routine = ['Cook','Bath','Eat','Sleep','Play'];
// let capital = ['Manila','Berlin','London','Paris'];
// console.log(routine);
// console.log(capital);



let tasks = ['Cook','Bath','Eat','Sleep','Play'];
let capitalCities = ['Tokyo','Madrid','Manila','Hanoi'];
console.log(tasks);
console.log(capitalCities);


//Arrays are used to store multiple related values in a single variable
  //type of variable is object
  //uses [] square brackets known as array literals
  //value pair = index number : element
  /* Syntax:
      let/const arrayName = [elementA,elementB,elementC]
  */

let hobbies = ["Play LOL","Read Book","Listen to Music","Code"];
console.log(typeof hobbies);


//examples
let grades = [98.5, 94.6, 90.8, 88.9];
let computerBrands = ['Acer','Asus','Lenovo','HP','Neo','Redfox','Gateway','Toshiba','Fujitsu'];

//not recommended
let mixedArr = [12, 'Asus', null, undefined, {}];
let arraySample = ['Cardo','One Punch Man',25000,false];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);
console.log(arraySample);


//Alternative way writing array

let myTasks = [
      'drink html',
      'eat javascript',
      'inhale css',
      'bake react'
  ];

//Create an array with values from variables

let username1 = 'gdragon22';
let username2 = 'gdino21';
let username3 = 'ladiesman217';
let username4 = 'transformers';
let username5 = 'noobmaster68';
let username6 = 'gbutiki78';

let guildMembers = [username1,username2,username3,username4,username5,username6];

console.log(guildMembers);
console.log(myTasks);

//[.length property]
  //number data type
  //allows us to get and set the total number of items or elements in array

console.log(myTasks.length); //4
console.log(capitalCities.length); //4


let blankArr = [];
console.log(blankArr.length);

//length property using strings

let fullName = "Cardo Dalisay"
console.log(fullName.length);

//length property can also set total number of array

myTasks.length = myTasks.length - 1;
console.log(myTasks.length);
console.log(myTasks);

//another example using decrementation

capitalCities.length --;
console.log(capitalCities);


// fullName.length = fullName.length -1;
// console.log(fullName.length); //13


let theBeatles = ["John","Paul","Ringo","George"];
theBeatles.length++;
console.log(theBeatles); //last element is empty

theBeatles[4] = fullName;
console.log(theBeatles); //5

theBeatles[theBeatles.length] = "Tanggol";
console.log(theBeatles);

//[Read from Arrays]
/*
  Access elements with the use of their index
  Syntax:
  arrayName[index]

*/

console.log(capitalCities[0]); //accessed the first element of the array

console.log(grades[100]); //undefined

let lakersLegends = ['Kobe','Shaq','Lebron','Magic','Kareem']
console.log(lakersLegends[2]); //3rd element
console.log(lakersLegends[4]); //5th element

//save array items in another variable
let currentLaker = lakersLegends[2];
console.log(currentLaker);

console.log("Array before assignemnt");
console.log(lakersLegends);
lakersLegends[2] = "Pau Gasol";
console.log("Array after re-assignment");
console.log(lakersLegends);

//Mini-Activity 2

let favoriteFoods = [
  "Tonkatsu",
  "Adobo",
  "Pizza",
  "Lasagna",
  "Sinigang"
];

favoriteFoods[3] = "Ramen";
favoriteFoods[4] = "Caldereta";

console.log(favoriteFoods);
console.log(favoriteFoods[3]);
console.log(favoriteFoods[4]);

//Mini-Activity 3

let blackMamba = [];

function findBlackMamba(index){
    return lakersLegends[index]
}

blackMamba = findBlackMamba(0);
console.log(blackMamba); //Kobe

//Mini-Activity 4


let theTrainers = ['Ash'];

function addTrainers(trainer){
  theTrainers[theTrainers.length] = trainer;
}

addTrainers("Misty");
addTrainers("Brock");
console.log(theTrainers);

//Access the last element of an array

let bullsLegends = ["Jordan","Pippen","Rodman","Rose","Kukoc"];
let lastElementIndex = bullsLegends.length - 1;

console.log(bullsLegends[lastElementIndex]); //Kukoc
console.log(bullsLegends[bullsLegends.length-1]);

//Add items into the array

let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);

newArr[1] = "Tifa Lockhart";
console.log(newArr);

newArr[newArr.length] = "Barrett Wallace";
console.log(newArr);

newArr[newArr.length-1] = "Aerith Gainsborough";
console.log(newArr);

//[Looping an array]

for (let index=0; index < newArr.length; index++){

    console.log(newArr[index])
}

let numArr = [5,12,30,48,40];

for (let index = 0; index<numArr.length; index++){

    if (numArr[index]%5 === 0){
        console.log(numArr[index] + " is divisible by 5!")
    }
    else {
        console.log(numArr[index] + " is not divisible by 5!")
    }
}

//[Multidimensional Array]

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard);


//Access elements of a multidimensional array

console.log(chessBoard[1][4]); //e2
console.log("Pawn moves to: " + chessBoard[1][5]);
console.log(chessBoard[7][0]);
console.log(chessBoard[5][7]);
console.log(chessBoard[0][0]);





