import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export default function Banner({ pageNotFound }) {

  if (pageNotFound) {
    return (
      <Row>
        <Col>
          <h1>Page Not Found</h1>
          <p>Go back to <a href="/homepage">homepage</a>.</p>
        </Col>
      </Row>
    );
  }

  return (
    <Row>
      <Col>
        <h1>Zuitt Coding Bootcamp</h1>
        <p>Opportunities for everyone, everywhere.</p>
        <Button variant="primary">Enroll Now!</Button>
      </Col>
      {/* You can add additional columns or rows for the homepage content here */}
    </Row>
  );
}





