import React from 'react';
import ReactDOM from 'react-dom/client';
// import './index.css';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
// import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);



//// If you want to start measuring performance in your app, pass a function
// // to log results (for example: reportWebVitals(console.log))
// // or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();



// const name = "Ada Lovelace";
// const user = {
//   firstName: "Jane",
//   lastName: "Smith"
// }

// function formatName(user){

//   return user.firstName + " " + user.lastName

// }

// //const element = <h1>Hello, {name}</h1>; //JSX - Javascript XML
// const element = <h1>Hello, {formatName(user)}</h1>

// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(element);