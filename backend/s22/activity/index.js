console.log("Hello World!")

/*
	
	1. Create a function called addNum which will be able to add two numbers.
		- Numbers must be provided as arguments.
		- Return the result of the addition.

*/

	function addNum(add1,add2) {
		
		console.log("Displayed sum of " + add1 + " and " + add2);
		return add1 + add2
	}

		let sum = addNum(1,2);
		console.log(sum);

/*
	   
	   Create a function called subNum which will be able to subtract two numbers.
	    - Numbers must be provided as arguments.
	    - Return the result of subtraction.

*/

		function subNum(sub1,sub2) {
		
		console.log("Displayed difference of " + sub1 + " and " + sub2);
		return sub1 - sub2
	}

		let difference = subNum(2,1);
		console.log(difference);

/*

	    Create a new variable called sum.
	     - This sum variable should be able to receive and store the result of addNum function.

*/



/*

	    Create a new variable called difference.
	     - This difference variable should be able to receive and store the result of subNum function.


*/


/*

	    Log the value of sum variable in the console.

*/



/*		
	    Log the value of difference variable in the console.

*/



/*

	2. Create a function called multiplyNum which will be able to multiply two numbers.
		- Numbers must be provided as arguments.
		- Return the result of the multiplication.

*/
		function multiplyNum(prod1,prod2) {
		
		console.log("Displayed product of " + prod1 + " and " + prod2);
		return prod1 * prod2
	}


		let product = multiplyNum(3,3)
		console.log(product);
/*
		Create a function divideNum which will be able to divide two numbers.
		- Numbers must be provided as arguments.
		- Return the result of the division.
*/

		function divideNum(quot1,quot2) {
		
		console.log("Displayed quotient of " + quot1 + " and " + quot2); 
		return quot1 / quot2
	}

		let quotient = divideNum(12,3)
		console.log(quotient);
/*
		Create a new variable called product.
		 - This product variable should be able to receive and store the result of multiplyNum function.
*/



/*
		Create a new variable called quotient.
		 - This quotient variable should be able to receive and store the result of divideNum function.
*/




/*
		Log the value of product variable in the console.
*/



/*		
		Log the value of quotient variable in the console.
*/




/*
	3. Create a function called getCircleArea which will be able to get total area of a circle from a provided radius.
		- a number should be provided as an argument.
		- look up the formula for calculating the area of a circle with a provided/given radius.
		- look up the use of the exponent operator.
		- return the result of the area calculation.

*/

		function getCircleArea(radius) {
			

			console.log("The result of getting the area of a circle with " + radius + " radius:")
			return radius**2*3.1416
		}

/*
		Create a new variable called circleArea.
		- This variable should be able to receive and store the result of the circle area calculation.
		- Log the value of the circleArea variable in the console.
*/

		let circleArea = getCircleArea(3);
		console.log(circleArea);


/*
	4. Create a function called getAverage which will be able to get total average of four numbers.
		- 4 numbers should be provided as an argument.
		- look up the formula for calculating the average of numbers.
		- return the result of the average calculation.
*/

		function getAverage(ave1,ave2,ave3,ave4) {
			
			console.log("The average of " + ave1 + ", " + ave2 + ", "+ ave3 + ", "+ ave4 + ":");
			return (ave1+ave2+ave3+ave4)/4
		}


/*
		Create a new variable called averageVar.
		- This variable should be able to receive and store the result of the average calculation
		- Log the value of the averageVar variable in the console.
*/

		let averageVar = getAverage(4,6,4,6);
		console.log(averageVar);
	
/*
	5. Create a function called checkIfPassed which will be able to check if you passed by checking the percentage of your score against the passing percentage.
		- this function should take 2 numbers as an argument, your score and the total score.
		- First, get the percentage of your score against the total. You can look up the formula to get percentage.
		- Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
		- return the value of the variable isPassed.
		- This function should return a boolean.
*/
		
			function checkIfPassed(yscore,tscore){
				console.log("Is " + yscore + "/" + tscore + " is a passing score?")
				return isPassed = (yscore/tscore)*100;

				 
			}


/*
		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

			let isPassingScore = checkIfPassed(50,100) >= 75 ;

			console.log(isPassingScore);

		




//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
	module.exports = {

		addNum: typeof addNum !== 'undefined' ? addNum : null,
		subNum: typeof subNum !== 'undefined' ? subNum : null,
		multiplyNum: typeof multiplyNum !== 'undefined' ? multiplyNum : null,
		divideNum: typeof divideNum !== 'undefined' ? divideNum : null,
		getCircleArea: typeof getCircleArea !== 'undefined' ? getCircleArea : null,
		getAverage: typeof getAverage !== 'undefined' ? getAverage : null,
		checkIfPassed: typeof checkIfPassed !== 'undefined' ? checkIfPassed : null,

	}
} catch(err){

}