//Contain all the endpoints for our application 

const express = require("express");

//Create a router instance that functions as a middleware and routing a system

const router = express.Router()

const taskController = require("../controllers/taskController")

router.get("/",(req,res)=>{

	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))

})


router.get("/:id",(req,res)=>{

	taskController.getSpecificTask(req.params.id).then(resultFromController => res.send(resultFromController))

})



router.post("/",(req,res)=>{

	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))

})

router.delete("/:id",(req,res)=>{

	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))

})

router.put("/:id",(req,res)=>{

	taskController.updateTaskName(req.params.id, req.body).then(resultFromController => res.send(resultFromController))

})

router.put("/:id/complete",(req,res)=>{

	taskController.updateTaskStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController))

})

module.exports = router;