console.log("Javascript Objects!");

//[Objects]
/*

  - Data type that is used to represent real world objects
  - Information stored in objects are represented in a "key:value" pair
  - A "key" is also mostly referred to as a "property" of an object
  - Different data types may also be stored in an object's property creating complex data structures

  Syntax:
  let objectName = {
  
    keyA:valueA,
    keyB:valueB

  }

*/

//Create objects using initialization

  let ninja = {
    name: "Naruto",
    village: "Konoha",
    occupation: "Hokage"
  }

  console.log("Result from creating objects using initializers/literal notation");
  console.log(ninja);
  console.log(typeof ninja);


  let dog = {
    name: "Whitey",
    color: "black",
    breed: "Chiwawa"
  }

//Create objects using constructor function

  /*

    Create re-usable function to create several objects that have same data structure
    Instance - a concrete occurence of any object which emphasize distinct / unique identity.

  */

  /*
      
      function ObjectName(keyA, keyB){
          this.keyA = keyA;
          this.keyB = keyB;

      }
      
      
      - "this" keyword allows to assign new object properties by associating the values from parameter

      - "new" operator creates a new instance of an object
  */

  function Laptop(name,manufactureDate){
    this.name = name;
    this.manufactureDate = manufactureDate;
  }

  let laptop1 = new Laptop('Lenovo', 2022);
  console.log("Result from creating objects using object constructor");
  console.log(laptop1);

  let myLaptop = new Laptop('Macbook Air', 2020);
  console.log("Result from creating objects using object constructor");
  console.log(myLaptop);

  //undefined since Laptop is invoked or called instead of creating "new" instance

  // let oldLaptop = Laptop('Portal R2E CCMC',1980);
  // console.log("Result from creating instance without new keyword");
  // console.log(oldLaptop);


  //Mini Activity 1


    let laptopA = new Laptop('ASUS ROG', 2025);
  console.log("Result from creating objects using object constructor");
  console.log(laptopA);

      let laptopB = new Laptop('Dell Inspiron', 2026);
  console.log("Result from creating objects using object constructor");
  console.log(laptopB);

        let laptopC = new Laptop('HP Matebook', 2027);
  console.log("Result from creating objects using object constructor");
  console.log(laptopC);


//Objects can be initialized even if empty

let computer = {};
let myComputer = new Object();
console.log(computer);
console.log(myComputer);


//[Access object Properties]

  // let myLaptop = new Laptop('Macbook Air', 2020);

  //dot notation
  console.log("Result: " + myLaptop.name);
    console.log("Result: " + myLaptop.manufactureDate);

  //square bracket notation
  console.log("Result: " + myLaptop['name']);
  console.log("Result: " + myLaptop['manufactureDate']);

//[Access array Objects]

/*
  Array objects can be accessed using square brackets
  Dot notation is preferred for objects
  Square notation is preferred for array

*/

let array = [laptop1, myLaptop];

//square bracket
console.log(array[0]['name']);

//dot notation
console.log(array[0].name);


//[Initialize, Add, Delete & Reassign Object Properties]

let car = {};

car.name = 'Honda Civic';
console.log("Result from adding properties using dot notation: ")
console.log(car);

// car.number = [1,2,3];
// console.log(car);

car['manufacture date'] = 2019
console.log(car['manufacture date']); //should not be used
//console.log(car.manufacture date);
console.log(car);
console.log(car.manufactureDate);//undefined

//Delete object properties

delete car['manufacture date'];
console.log('Result of deleting properties: ')
console.log(car);

//Re-assign object properties

car.name = 'Dodge Charger R/T'
console.log('Result of re-assigning properties:')
console.log(car);


//[Object Methods]

/*

  A method is a function which is a property of an object

*/

  let person = {
      name: 'Cardo',
      talk: function (){
          console.log('Hello my name is ' + this.name);
      }
  }
  
console.log(person);
console.log('Result of object methods: ');
person.talk();

person.walk = function(){
    console.log(this.name + ' walked 25 steps forward!')
}

person.walk();

let friend = {
    firstName: 'Nami',
    lastName: 'Misko',
    address: {
        city: 'Tokyo',
        country: 'Japan',
    },
    emails: ['nami@sea.com','namimisko@gmail.com'],
    introduce: function(){
        console.log('Hello! My name is ' + this.firstName + ' ' + this.lastName);
    }
}


friend.introduce();



//[Real World Application of Objects!]


/*

  Scenario:
  1. A game that would have several pokemon that would interact to each other
  2. Every pokemon have same set of stats, properties and functions

*/

//Object Literals

let myPokemon = {
      name: 'Pikachu',
      level: '3',
      health: 100,
      attack: 50,
      tackle: function(){
          console.log("This Pokemon tackled Target Pokemon!")
          console.log("Target Pokemon's health is now reduced to Target Pokemon Health")
      },
      faint: function(){
          console.log("Pokemon fainted")
      }
}

console.log(myPokemon);
myPokemon.faint();

//Using Constructor

  function Pokemon(name,level){

    //Properties
      this.name = name;
      this.level = level;
      this.health = 2 * level;
      this.attack = level;

    //Methods
      this.tackle = function(target){
            console.log(this.name + ' tackled ' + target.name);
            console.log(target.name + "'s health is now reduced to _targetPokemonHealth_")
      }
      this.faint = function(){
          console.log(this.name + ' fainted.');
      }
  }

  let pikachu = new Pokemon("Pikachu",16);
  let rattata = new Pokemon("Rattata",8);
  pikachu.tackle(rattata);
  rattata.faint();


  



