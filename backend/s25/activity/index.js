console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals

// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method

let trainer = {
      name: 'Ash Ketchum',
      age: 17,
      pokemon: ['Pikachu','Squirtle','Charmander','Balbasaur'],
      friends: {
            list: ['Friend1','Friend2','Friend3']
      }
}
console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of bracket notation:");
console.log(trainer['pokemon']);

trainer.talk = function(){
    let message;
    return message = "Pikachu! I choose you!"
}

console.log("Result of talk method:")
console.log(trainer.talk());


// Create a constructor function called Pokemon for creating a pokemon

  function Pokemon(name,level){


      this.name = name;
      this.level = level;
      this.health = 4 * level;
      this.attack = 2 * level;

      this.faint = function(target){
          console.log(target.name + ' fainted.');
      }
      this.tackle = function(target){
            console.log(this.name + ' tackled ' + target.name);
            target.health = target.health - this.attack;
            if (target.health <= 0){
                return this.faint(target);
                
            }
            else {
                return "New HP of " + target.name + " is " + target.health + "!"; 
            }
             

      }

  }
// Create/instantiate a new pokemon

let pokemon1 = new Pokemon('Pikachu',5);
console.log(pokemon1);

// Create/instantiate a new pokemon

let pokemon2 = new Pokemon('Geodude',5);
console.log(pokemon2);

// Create/instantiate a new pokemon

let pokemon3 = new Pokemon('Mewtwo',10);
console.log(pokemon3);

// Invoke the tackle method and target a different object

console.log(pokemon1.tackle(pokemon2));


// Invoke the tackle method and target a different object

//pokemon1.tackle(pokemon2);
// pokemon1.tackle(pokemon3);
// pokemon1.tackle(pokemon3);
// pokemon1.tackle(pokemon3);







//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}