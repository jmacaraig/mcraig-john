console.log("Hi from index.js");

//fetch() methods has one argument by default, the url
//the url is a representative address of accessing a resource/data in another machine

//jsonplaceholder url can be ised from sample server

//the .then() method will allow us to process the data we retrieve using fetch in another function

//response is a parameter to indicate processing of response from server

//response.json() is a method to convert incoming data as proper JS object

//we can add .then() method to do something with processed response

fetch("https://jsonplaceholder.typicode.com/posts").then(response=>response.json()).then(data=>{
    // console.log(data)
    //trigger showposts function after fetching data from server
    showPosts(data);
});

// fetch("https://jsonplaceholder.typicode.com/posts")
// .then(function(response) {return response.json();})
// .then(function(data) {console.log(data);});

// const showPosts = function(posts){
//     //add each posts as a string
//     let postEntries = " ";
//     posts.forEach(function(post){

//         postEntries += `

//             <div id="post-${post.id}">
//                 <h3 id="post-title-${post.id}">${post.title}</h3>
//                 <p id="post-body-${post.id}">${post.body}</p>
//                 <button onclick = "deletePost('${post.id}')">Delete</button>
//             </div>

//         `

//     })

// }



const showPosts = (posts)=>{
    //add each posts as a string
    let postEntries = " ";
    posts.forEach((post)=>{

        console.log(posts);

        postEntries += `

            <div id="post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onclick = "editPost('${post.id}')">Edit</button>
                <button onclick = "deletePost('${post.id}')">Delete</button>
            </div>
            
        `

    })

    // console.log(postEntries);
    document.querySelector('#div-post-entries').innerHTML = postEntries;

}

    document.querySelector('#form-add-post').addEventListener('submit',(e)=>{
        event.preventDefault(); //prevent auto reload(default) when submit is used

        let titleInput = document.querySelector('#txt-title');
        let bodyInput = document.querySelector('#txt-body');
        // console.log(titleInput.value);
        // console.log(bodyInput.value);


        // console.log("Hello! The form has been submitted!")


        //Adding, updating and deleting data to a server requires passing another argument to fetch() method that contains other details

        //fetch("<URL>",{options})
            //options object should look like this:
                //method: tells the server what we want to do with HTTP methods
                    //GET(retrieve/read) = getting data in a server
                    //POST(add) = add data in server
                    //PUT(update) = updating data in server
                    //DELETE(delete) = deleting data in server

                //body: contains main content we want to send to our servers

                //headers: contains other details we need to send on servers

        fetch("https://jsonplaceholder.typicode.com/posts",{
            method: 'POST',
            body: JSON.stringify({
                title: titleInput.value,
                body: bodyInput.value,
                userID: 1
            }),
            headers: {'Content-type':'application/json'}
        })
        .then(function(response){
            return response.json();
        })
        .then(function(data){
            console.log(data);
            alert('Successfully added!')

            //clear the input elements after submission
            titleInput.value = null;
            bodyInput.value = null;
        })

        alert('Successfully added!')

    }) 

    // document.querySelector('#form-add-post').addEventListener('submit',function(){
    //     event.preventDefault();
    //     console.log("Hello! The form has been submitted!")

    // })



//Edit post


const editPost = function(id){

    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;
    document.querySelector('#btn-submit-update').removeAttribute('disabled')

}


//Update Post

document.querySelector('#form-edit-post').addEventListener('submit',(e)=>{
    e.preventDefault();


        fetch("https://jsonplaceholder.typicode.com/posts/1",{
            method: 'PUT',
            body: JSON.stringify({
                id: document.querySelector('#txt-edit-id').value,
                title: document.querySelector('#txt-edit-title').value,
                body: document.querySelector('#txt-edit-body').value,
                userID: 1
            }),
            headers: {'Content-type':'application/json'}
        })
        .then(function(response){
            return response.json();
        })
        .then(function(data){
            console.log(data);
            alert('Successfully added!')

            //clear the input elements after submission

            document.querySelector('#txt-edit-id').value = null;
            document.querySelector('#txt-edit-title').value = null;
            document.querySelector('#txt-edit-body').value = null;
            document.querySelector('#btn-submit-update').setAttribute('disabled',true);
        })




})

//Activity

const deletePost = function(id){

    fetch('https://jsonplaceholder.typicode.com/posts',{
        method: 'DELETE'
    });
    document.querySelector(`#post-${id}`).remove();

}


document.querySelector(`#delete-all`).addEventListener('click',()=>{
    document.querySelector(`#div-post-entries`).innerHTML = '';
    alert("All posts Deleted!");
});