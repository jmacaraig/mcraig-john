import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from "../UserContext";
import Swal from 'sweetalert2';


export default function Login(props){

    const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState(false);

	function userLogin(e){

		e.preventDefault();

		fetch('http://localhost:4000/users/login',{
			method: 'POST',
			headers:{
				"Content-Type":"application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})

		})
		.then(res=>res.json())
		.then(data=>{

			// console.log(data)

			if (typeof data.access !== 'undefined'){

				localStorage.setItem('token',data.access)

				retrieveUserDetails(data.access);

				setEmail("");
				setPassword("");

				// alert("Logged in Succesfully!")

				Swal.fire({
					title: "Login Successful",
					icon: "Success",
					text: "Welcome to Zuitt!"
				})

			} else {

				// alert("Please try again.")
				Swal.fire({
					title: "Authentication failed",
					icon: "Error",
					text: "Check your detials and login again."
				})

			}

		})

	}


	const retrieveUserDetails = (token) =>{

		fetch('http://localhost:4000/users/details',{
			method: 'POST',
			headers:{
				Authorization: `Bearer ${token}`
			}

		})
		.then(res => res.json())
		.then(data => {

			// console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})

		})

}

	useEffect(()=>{

		if(email !== "" && password !== ""){

			setIsActive(true)

		}else{

			setIsActive(false)

		}

	},[email,password])


	return (
		(user.id !== null) ?
            <Navigate to="/courses" />
        :

		<Form onSubmit={(e)=>userLogin(e)}>
			<h1 className = "my-5 text-center">Login</h1>

			<Form.Group>
				<Form.Label>Email:</Form.Label>
				<Form.Control type="email" placeholder="Enter Email" required value={email} onChange={e=>{setEmail(e.target.value)}}/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password:</Form.Label>
				<Form.Control type="password" placeholder="Enter password" required value={password} onChange={e=>{setPassword(e.target.value)}}/>
			</Form.Group>

			{
				isActive ?
				<Button variant="success" type="submit" id="submitBtn">Login</Button>
				:
				<Button variant="secondary" type="submit" id="submitBtn">Login</Button>
			}

		</Form>
		

	)

}