import { Button, Modal, Form } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import Swal from 'sweetalert2';



export default function ArchiveCourse({courseId, courseActive, fetchData}) {


	const archiveToggle = (courseId) => {

		fetch(`${ process.env.REACT_APP_API_URL}/courses/${courseId}/archive`,{
				
			method:'PUT',
			headers:{
				'Content-Type':'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
				}	
		})
		.then(res => res.json())
    	.then(data => {
      	
	      if (data === true) {
	        fetchData();
	        Swal.fire({
	          title: "Success",
	          icon: "success",
	          text: "Course Archived!" // Correct the success message
	        });
	      } else {
	        fetchData();
	        Swal.fire({
	          title: "Error",
	          icon: "error",
	          text: "Please try again!"
	        });
	      }
	    });

	}

	const activateToggle = (courseId) => {

		fetch(`${ process.env.REACT_APP_API_URL}/courses/${courseId}/activate`,{
				
			method:'PUT',
			headers:{
				'Content-Type':'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
				}	
		})
		.then(res => res.json())
    	.then(data => {
      	
	      if (data === true) {
	        fetchData();
	        Swal.fire({
	          title: "Success",
	          icon: "success",
	          text: "Course Activated!" // Correct the success message
	        });
	      } else {
	        fetchData();
	        Swal.fire({
	          title: "Error",
	          icon: "error",
	          text: "Please try again!"
	        });
	      }
	    });

	}

	return (

		<>
			{
			(courseActive === true)
			?
			<Button variant="danger" size="sm" onClick={()=>archiveToggle(courseId)}>Archive</Button>
			:
			<Button variant="success" size="sm" onClick={()=>activateToggle(courseId)}>Activate</Button>

			}
		</>


	)

}

