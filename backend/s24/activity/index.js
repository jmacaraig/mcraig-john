console.log("Hello World");


//Objective 1
//Add code here
//Note: function name is numberLooper

function numberLooper(num){
    let message;
    for(let count=num; count>0; count--){
        if (count <= 50){
            return message = "The current value is at "+ count + ". Terminating the loop.";
            break;
        }
        if (count%10 === 0){
            message = "The number is divisible by 10. Skipping the number."
            console.log(message);
            continue;
        }
        if (count%10 === 5){
            console.log(count);
            continue;
        }
      }  
    }



//Objective 2
let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

//Add code here

    for (let i=0; i < string.length; i++){

      if (
      string[i] == "a" ||
      string[i] == "e" ||
      string[i] == "i" ||
      string[i] == "o" ||
      string[i] == "u"
      ){
        continue;
      }
      else {
        filteredString += string[i];
      }
    }
    console.log(filteredString);
      



//Do not modify 
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        filteredString: typeof filteredString !== 'undefined' ? filteredString : null,
        numberLooper: typeof numberLooper !== 'undefined' ? numberLooper : null
    }
} catch(err){

}