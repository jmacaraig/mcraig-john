console.log("Hi from index.js");

var nameVar = "Camille";

if (true){
    var nameVar = "Hi";

}

var nameVar = "C";

let name1 = "Cee";

if (true){
    let name1 = "Hello";

}

// let name1 = "Hello World";

console.log(name1);



//[**] - exponential operator

    const firstNum = 8**2;

    console.log(firstNum);

    const secondNum = Math.pow(8,2);
    console.log(secondNum); //64

let string1 = 'fun';
let string2 = 'Bootcamp';
let string3 = 'Coding';
let string4 = 'JavaScript';
let string5 = 'Zuitt';
let string6 = 'Love';
let string7 = 'Learning';
let string8 = 'I';
let string9 = 'is';
let string10 = 'in';


let concatSentence = string8 + " " + string6 + " " + string5;
console.log(concatSentence);

//Recommended from ES6 standard

let concatSentence2 = `${string8} ${string6} ${string5} ${string3} ${string2}!`
console.log(concatSentence2);



let sentence = concatSentence.concat(" ",concatSentence2);
console.log(sentence);


//${} is a place holder that is used to embed JS expressions creating strings using template literals

let name = "Carding";

let message = `Hello ${name}! Welcome to programming!`;
console.log(`Message with template literals: ${message}`);

const anotherMessage = `

    ${name} attended a math competition.

    He won it by solving the problem 8**2 with the answer of ${firstNum}!

`;


console.log(anotherMessage);

const interestRate = .1;
const principal = 1000;
console.log(`The interest on your savings account is: ${principal * interestRate}`);

let dev = {
    name: "Peter",
    lastName: "Parker",
    occupation: "Web Developer",
    income: 50000,
    expenses: 60000

};

console.log(`${dev.name} is a ${dev.occupation}.`);
console.log(`His income is ${dev.income} and expenses at ${dev.expenses}. His current balance is ${dev.income - dev.expenses}`);


//[Array Destructuring]

/*

    Allows to unpack elements in array to variables
    Allows to name array elements with variables instead of index number
    Readability
*/

const fullName = ["Juan","Dela","Cruz"];

//Pre-Array Destructuring

console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);

//Array Destructuring

const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);
console.log(`Hello ${firstName} ${middleName} ${lastName}! I was enchanted to meet you!`)

console.log(fullName);


let fruits = ["Mango","Grapes","Guava","Apple"];
let [fruit1, ,fruit3,] = fruits;
console.log(fruit1);
console.log(fruit3);

let kupunanNiEugene = ["Eugene","Alfred","Vincent","Dennis","Taguro","Master Jeremaiah"];

let [gF1,gF2,gF3,gF4,,gF6] = kupunanNiEugene;
// console.log(gF1);
// console.log(gF2);
// console.log(gF3);
// console.log(gF4);
// console.log(gF6);
console.log(`
    ${gF1}
    ${gF2}
    ${gF3}
    ${gF4}
    ${gF6}

`);


//Object Destructuring

/*

    Allows to unpack properties of object to variables
 
    Syntax:
        let/const {propertyName, propertyName, propertyName} = object;
 
*/

const person = {
    givenName: "Jane",
    maidenName: "Dela",
    familyName: "Cruz"

}

//pre-object destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);
console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again!`)

const {givenName, maidenName, familyName} = person;

console.log(givenName);
console.log(maidenName);
console.log(familyName);
console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again!`)

// const {n1, n2, n3} = person; //only real property name will work

// console.log(n1); //undefined
// console.log(n2); //undefined
// console.log(n3); //undefined
// console.log(`Hello ${n1} ${n2} ${n3}! It's good to see you again!`)


function getFullName ({givenName, maidenName, familyName}){
    console.log(`${givenName} ${maidenName} ${familyName}`)
}

getFullName(person);


//[Arrow Functions]

/*

    Compact syntax to traditional functions
    DRY Concept (Don't Repeat Yourself) principle where name is not needed since functions are only used once


*/

function displayMsg(){

    console.log("Hi");

}

displayMsg();

//function expression (function inside variable)

let dislayHello = ()=>{
    console.log("Hello World!")
}

dislayHello();


// function printFullName (firstName, middleInitial, lastName){
//     console.log(`${firstName} ${middleInitial} ${lastName}`);

// }

// printFullName("John","D","Smith");


let printFullName = (firstName, middleInitial, lastName)=>{

    console.log(`${firstName} ${middleInitial} ${lastName}`);

}

printFullName("John","D","Smith");

//Arrow function with Loops

const students = ["John","Jane","Natalia","Jobert","Joe"];

students.forEach(function(student){
    console.log(`${student} is a student!`)
});

students.forEach((student)=>{
    console.log(`${student} is a student! (from arrow)`)
});


//[Implicit Return Statement]

/*
    Instance when we can omit the return statement

*/

    function add(x,y){

        return x+y;

    }

    let total = add(1,2);
    console.log(total);//3


    const addNew = (x,y)=>{
        return x + y;
    }

    let totalNew = add(1,2);
    console.log(totalNew);

    const addOneLine = (x,y)=>x + y;

    let totalOneLine = addOneLine(1,2);
    console.log(totalOneLine);


//[Default Function Argument Value]

    // const greet = (name) => `Goodmorning, ${name}!`

    // console.log(greet());
    // console.log(greet("John"));

    const greet = (name = 'User') => `Goodmorning, ${name}!`

    console.log(greet());
    console.log(greet("John"));

//[Class-Based Object Blueprint]

/*

    Allows creation of object using classes as blueprint

*/


//Creating a Class
    //Constructor is a special method of a class for creating an object for that class


class Car {
    constructor(brand="Any",name="Any",year="Any"){
        this.brand = brand;
        this.name = name;
        this.year = year;

    }
}

let myCar = new Car();

console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);


const myCar2 = new Car("Toyota","Vios",2021);
console.log(myCar2);


//Traditional functions vs Arrow functions as methods


let character1 = {

    name: "Cloud Strife",
    occupation: "Soldier",
    greet: ()=>{

        //this keyword refers to current object where method is
        console.log(this); //global window object or global scripts or global variables
        console.log(`Hi! I am ${this.name}`);

    },
    introjob: function(){

        //this keyword refers to current object where method is
        console.log(`Hi! I am ${this.name}. I am a ${this.occupation}`);

    }

}

character1.greet();
character1.introjob();


class Character{
        constructor(name,role,strength,weakness){
            this.name = name;
            this.role = role;
            this.strength = strength;
            this.weakness = weakness;
            this.introduce = ()=>{
                console.log(`Hi! I am ${this.name}!`)
            }
        }

}

let myChar = new Character("Earthling","Tank","Fight on Land","Water")

console.log(myChar);
myChar.introduce();