import {useState, useEffect, useContext} from 'react';
import {Link, NavLink} from 'react-router-dom';
import UserContext from "../UserContext";

import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';



export default function AppNavbar () {

	const { user, setUser } = useContext(UserContext);

	// const [user,setUser] = useState(localStorage.getItem("token"))
	// console.log(user);

	return (
		<Navbar bg="light" expand="lg">
			<Container fluid>
				<Navbar.Brand /*href="#home"*/ as={Link} to="/">
					Zuitt Booking
					{/*<h1>App Navbar</h1>*/}
				</Navbar.Brand>
				<Navbar.Toggle aria-controls = "basic-navbar-nav"/>
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ms-auto">
						<Nav.Link /*href="#home"*/ as={ NavLink } to="/" exact>Home</Nav.Link>
						<Nav.Link /*href="#link"*/ as={ NavLink } to="/courses" exact>Courses</Nav.Link>

						{(user.id !== null)?
						           user.isAdmin 
						           ?
						           <>
						               <Nav.Link as={Link} to="/addCourse">Add Course</Nav.Link>
						               <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
						           </>
						           :
						           <>
						               <Nav.Link as={Link} to="/profile">Profile</Nav.Link>
						               <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
						           </>
						       : 
						           <>
						               <Nav.Link as={Link} to="/login">Login</Nav.Link>
						               <Nav.Link as={Link} to="/register">Register</Nav.Link>
						           </>

						}

						
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>

	)

}