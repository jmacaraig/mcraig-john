console.log("Array Non-Mutator and Iterator Methods");

//Non-Mutator Methods
    //functions that do not modify and change an aray after they are created
    //Do not manipulate the original array performing tasls
        //returning elements
        //combining array
        //printing the output


let countries = ['US','PH','CAN','SG','TH','PH','FR','DE'];

//indexOf()
    //returns index number of first matching element found in array
    //if no match is found, result would be -1

let firstIndex = countries.indexOf('PH');
console.log("Result of indexOf method: "+ firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log("Result of indexOf method: "+ invalidCountry);


//lastIndexOf
    //returns the index number of the last matching element found
    //if no match is found, result will be -1


let lastIndex = countries.indexOf('PH');
console.log("Result of lastIndexOf method: "+ lastIndex);

let lastIndexStart = countries.indexOf('PH', 6); //result is 5, 6 is starting search number
console.log("Result of lastIndexOf method: "+ lastIndexStart);

//slice()
    //returns portion or a slice of array and returns new array

console.log(countries);

let slicedArrayA = countries.slice(2); //['CAN','SG','TH','PH','FR','DE']; first number is starting position
console.log("Result of slice method:");
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2,4); //['CAN','SG']; 2nd number is starting of deletion or slice
console.log("Result of slice method:");
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-3); //['PH','FR','DE'];
console.log("Result of slice method:");
console.log(slicedArrayC);

//toString()
    //returns an array as a string seprated by commas
    //creates new variable

let stringArray = countries.toString();
console.log("Result from toString method:");
console.log(stringArray);

let sampArr = [1,2,3];
console.log(sampArr.toString());


//concat()
    //combines 2 arrays and returns the combined result

let taskArrayA = ['drink html','eat javascript'];
let taskArrayB = ['inhale css','breathe mongodb'];
let taskArrayC = ['get git','be node'];

let tasks = taskArrayA.concat(taskArrayB);
console.log('Result from concat method:');
console.log(tasks);

let allTasks = taskArrayA.concat(taskArrayB,taskArrayC);
console.log('Result from concat method:');
console.log(allTasks);

let combinedTasks = taskArrayA.concat('smell express','throw react')
console.log('Result from concat method:');
console.log(combinedTasks);


//join()
    //returns an array as a string seperated by specified seperator (string data type)

let users = ['John','Jane','Joe','James'];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));


//Iteration Methods
    //These are loops designed to perform repetitive tasks on arrays
    //Array iteration works with a function supplied as an argument

//forEach()
    //Similar to a for loop that iterates on each array element
    //variable names should be plural
    //function parameter should be singular term of array name
    //forEach() does not return anything

console.log(allTasks);


allTasks.forEach(function(task){
    console.log(task);
});

let filteredTasks = [];

allTasks.forEach(function(task){

    //console.log(task);

    if (task.length>10){
        filteredTasks.push(task)
    }

});

console.log("Result of filteredTasks:");
console.log(filteredTasks);

//map()
    //iterates on each element and returns new array with different value
    //requires use of return

let numbers = [ 1, 2, 3, 4, 5 ];

let numberMap = numbers.map(function(number){
    return number*number;
});

console.log('Original Array:')
console.log(numbers);
console.log('Result of map method:')
console.log(numberMap);


//map() vs forEach()

// let numberForEach = numbers.forEach(function(){
//     return number*number;
// });

// console.log(numberForEach);


//every()
    //use function
    //checks if all elements in array meets condition
    //returns only one boolean value for all elements (true or false)

let allValid = numbers.every(function(number){
    return (number < 3);
});

console.log('Result of every method:');
console.log(allValid); //false because not all is < 3


//some()
    //checks if at least one element in the array meets condition

let someValid = numbers.some(function(number){
    return (number < 2); 
});

console.log('Result of some method:');
console.log(someValid); //true because some is < 2


//filter()
    //use function
    //returns a new array that contains elements which meets the given condition
    //returns an empty array if no elements were found


let filterValid = numbers.filter(function(number){
    return (number < 3); //returns numbers that are less than 3 
});

console.log('Result of filter method:');
console.log(filterValid); //[1,2]

let nothingFound = numbers.filter(function(number){
    return (number = 0); //return numbers equal to 0
});

console.log('Result of filter method:');
console.log(nothingFound); //empty



let filteredNumbers = [];

numbers.forEach(function(){

    if (number < 3){
        filteredNumbers.push(number);
    }

});

console.log('Result of forEach method:');
console.log(filteredNumbers);


let products = ['Mouse','Keyboard','Laptop','Monitor']


//includes()
    //checks if the argument passed can be found in array

let productFound1 = products.includes("Mouse");
console.log(productFound1);

let productFound2 = products.includes("Headset");
console.log(productFound2);


//Array Methods Chain


let filteredProducts = products.filter(function(product){

    return product.toLowerCase().includes('a');

});

console.log(filteredProducts);


//reduce()
    //evaluates elements from left to right and returns/reduces the array into single value
    //accumulator and current value parameter
    //function(accumulator,current value)
    //accumulator stores the result
    //current value is thhe current element being evaluated

console.log(numbers); //[ 1, 2, 3, 4, 5 ]
    //1 + 2 = 3
    //3 + 3 = 6
    //6 + 4 = 10
    //10 + 5 = 15

let iteration = 0

let reducedArray = numbers.reduce(function(acc,curr){

    console.warn('Current Iteration:' + ++iteration);
    console.log('Accumulator: ' + acc);
    console.log('Current Value: ' + curr)
    return acc+curr;

});

console.log("Result of reduce method: " + reducedArray);


//reduce string arrays

let list = ['Hello','From','The','Other','Side'];

let reducedJoin = list.reduce(function(acc,curr){
    console.log('Accumulator: ' + acc);
    console.log('Current Value: ' + curr)
    return acc+" "+curr;

});

console.log("Result of reduce method: " + reducedJoin);