import { BrowserRouter as Router, useLocation as location } from 'react-router-dom';
import {Route, Routes} from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import {useState, useEffect} from 'react'; 


// import logo from './logo.svg';

import { UserProvider } from "./UserContext";
import AppNavbar from './components/AppNavbar.js';
import AddCourse from './components/AddCourse.js';
import Banner from './components/Banner.js';
import Home from './pages/Home.js';
import Courses from './pages/Courses.js';
import CourseView from './pages/CourseView.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import Profile from './pages/Profile.js';

import './App.css';

function App() {

  const [user, setUser] = useState({
    // token: localStorage.getItem('token')
    id:null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(()=>{

    // console.log(user);
    // console.log(localStorage);

    fetch('http://localhost:4000/users/details',{
      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }

    })
    .then(res => res.json())
    .then(data => {

      // console.log(data);

      if(typeof data._id !== "undefined"){

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
      })

      } else {

        setUser({
          id: null,
          isAdmin: null
        })

      }
    })

  },[user])

  const pageNotFound = !(
    location.pathname === '/' ||
    location.pathname === '/courses' ||
    location.pathname === '/register' ||
    location.pathname === '/login' ||
    location.pathname === '/logout'
  );

  return (
    // <div className="App">
    //   <h1>Hello World</h1>
      <UserProvider value={{ user, setUser, unsetUser}}>
      <Router>
      <Container fluid>

        <AppNavbar/>

        <Routes>
          <Route path="/" element={<Home/>} />
          <Route path="/profile" element={<Profile/>} />
          <Route path="/homepage" element={<Banner/>} />
          <Route path="/courses" element={<Courses/>} />
          <Route exact path="/courses/:courseId" element={<CourseView/>} />
          <Route path="/register" element={<Register/>} />
          <Route path="/login" element={<Login/>} />
          <Route path="/logout" element={<Logout/>} />
          <Route path="/addCourse" element={<AddCourse/>} />
          <Route path="*" element={<Banner pageNotFound />} />

        </Routes>
{/*     <Home/>
        <Courses/>
        <Register/>
        <Login/>*/}
      </Container>
      </Router>
      </UserProvider>
 
      
    // </div>
  );
}

export default App;


/*      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>*/