// import logo from './logo.svg';
import './App.css';
import AppNavbar from './components/AppNavbar.js';
import Home from './pages/Home.js';
import Container from 'react-bootstrap/Container';

function App() {
  return (
    // <div className="App">
    //   <h1>Hello World</h1>
    <>  
      <AppNavbar />
      <Container>
        <Home/>
      </Container>
    </>
      
    // </div>
  );
}

export default App;


/*      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>*/