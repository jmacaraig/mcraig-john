import Banner from '../components/Banner.js';
import Highlights from '../components/Highlights.js';
import FeaturedCourses from '../components/FeaturedCourses.js';
// import PreviewCourses from '../components/PreviewCourses.js';

// export default function Home () {

// 	return (
// 	<>  
//       <Banner />
//       <Highlights />
//     </>

// 	)

// }

export default function Home() {


const data = {
    title: "Zuitt Coding Bootcamp",
    content: "Opportunities for everyone, everywhere",
    destination: "/courses",
    label: "Enroll now!"
}

	return (
		<>
			<Banner data={data} />
			<FeaturedCourses />
			<Highlights />
		</>
		
	)
}