const Task = require("../models/tasks");

module.exports.getAllTasks = () => {

	return Task.find({}).then(result=>{

		return result;

	})

}

module.exports.createTask = (requestBody) => {

	let newTask = new Task ({
		name: requestBody.name,
	})

	return newTask.save().then((task,error)=>{

		if (error){
				console.log(error);
				return false;
			} else {
				return task
			}

	})

}

module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndRemove(taskId).then((removedTask,err)=>{

		if(err){
			console.log(err);
			return false;
		} else {

			return removedTask;

		}

	})

}

module.exports.updateTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result,err)=>{

		if(err){
			console.log(err);
			return false;
		} else {

			result.name = newContent.name;

			return result.save().then((updatedTask,err)=>{

				if (err){
					console.log(err);
					return false;
				} else {
					return updatedTask;
				}

			})

		}

	})

}
