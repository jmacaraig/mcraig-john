//CONTROLLER

const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth")
const User = require("../models/User");

module.exports.addCourse = (req,res) => {
console.log(req.body)
	let newCourse = new Course ({

		name : req.body.name,
		description : req.body.description,
		price : req.body.price

	})

	return newCourse.save().then((course,error)=>{

		if (course){
			res.send(true)
			return
		}
		else {
			res.send(false)
			return
		}

	
	})

	.catch(err=>err) 
}

module.exports.getAllCourses = (req,res)=>{

	return Course.find({}).then(result=>{

		return res.send(result)

	})

}

//getAllActiveCourses

module.exports.getAllActiveCourses = (req,res)=>{

	return Course.find({

		isActive: true

	}).then(result=>{

		return res.send(result)

	})

}


module.exports.getCourse = (req,res)=>{

	return Course.findById(req.params.courseId).then(result=>{

		return res.send(result)

	})

}


module.exports.updateCourse = (req,res)=>{

	let updatedCourse = {
			name: req.body.name,
			description: req.body.description,
			price: req.body.price
	}

	return Course.findByIdAndUpdate(req.params.courseId, updatedCourse).then((course,error)=>{

		if (error){
			return res.send(false);
		}else{
			return res.send(true);
		}

	})

	}


module.exports.archiveCourse = (req,res)=>{

	let archive = {

		isActive: req.body.isActive

	}

	return Course.findByIdAndUpdate(req.params.courseId, archive).then((course,error)=>{

		if (error){
			return res.send(false);
		}else{
			return res.send(true);
		}

	})

}

module.exports.activateCourse = (req,res)=>{

	let activate = {

		isActive: req.body.isActive

	}

	return Course.findByIdAndUpdate(req.params.courseId, activate).then((course,error)=>{

		if (error){
			return res.send(false);
		}else{
			return res.send(true);
		}

	})

}


module.exports.searchCoursesByName = async (req, res) => {
  try {
    const { courseName } = req.body;

    // Use a regular expression to perform a case-insensitive search
    const courses = await Course.find({
      name: { $regex: courseName, $options: 'i' }
    });

    res.json(courses);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

module.exports.getEmailsOfEnrolledUsers = async (req, res) => {
  const courseId = req.params.courseId; // Use req.params instead of req.body for URL parameters

  try {
    // Find the course by courseId
    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ message: 'Course not found' });
    }

    // Get the userIds of enrolled users from the course
    const userIds = course.enrollees.map(enrollee => enrollee.userId);

    // Find the users with matching userIds
    const enrolledUsers = await User.find({ _id: { $in: userIds } }); // Change 'users' to 'userIds'

    // Extract the emails from the enrolled users
    const emails = enrolledUsers.map(user => user.email); // Change 'enrolledStudents' to 'enrolledUsers', use map instead of forEach

    res.status(200).json({ userEmails: emails }); // Change 'userEmails' to 'emails'
  } catch (error) {
    res.status(500).json({ message: 'An error occurred while retrieving enrolled users' });
  }
};


module.exports.searchCoursesByPrice = async (req, res) => {
  try {
    const { minPrice, maxPrice } = req.body;

    // Query courses within the given price range
    const courses = await Course.find({
      price: { $gte: minPrice, $lte: maxPrice }
    });

    res.json(courses);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};