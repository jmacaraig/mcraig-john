console.log("Hello World");

/*
    1. Create the following variables to store to the following user details:

    Variable Name - Value Data Type:  */
    
    let firstName = 'John';
    let lastName = 'Mcraig';
    let age = 18;
    let hobbies = ['Camping','Coding','Sleeping'];
    let workAddress = {
    	    houseNumber: '143',
            street: 'No Hate St.',
            city: 'Only Love City',
            state: 'Earth',
    }

 /*
    The hobbies array should contain at least 3 hobbies as Strings.
    
    The work address object should contain the following key-value pairs:

            houseNumber: <value>
            street: <value>
            city: <value>
            state: <value>

    Log the values of each variable to follow/mimic the output.

    Note: strictly follow the variable names. */


    //Add your variables and console log for objective 1 here:

    // let Objective1A = "Hi, I am "+firstName+" "+lastName;
    // let Objective1B = ", "+age+" years old. ";

    console.log("First Name: "+firstName);
    console.log("Last Name: "+lastName);
    console.log("Age: "+age);
    console.log("Hobbies:");
    console.log(hobbies);
    console.log("Work Address:");
    console.log(workAddress);



/*          
    2. Debugging Practice - Identify and implement the best practices of creating and using variables 
       by avoiding errors and debugging the following codes:

            -Log the values of each variable to follow/mimic the output.

        Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
*/  

    let fullName = "Steve Rogers";
    console.log("My full name is: " + fullName); //1. Replaced name to fullName and corrected string as per expected output

    let currentAge = true; 
    console.log("My current age is: " + 40); //1. currentAg replaced since it is Boolean and is not the expected output
    
    let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"]; //1. Added commas and double quotes to match and seperate array values
    console.log("My Friends are: ")
    console.log(friends);

    let profile = {

        username: "captain_america", //1. Added semi-colon and comma
        fullName: "Steve Rogers", //1. Replaced semi-colon with comma in the end of line 
        						  //2. Single quote on the end was changed to double quotes
        age: 40,
        isActive: false,

    }
    console.log("My Full Profile: ");
    console.log(profile);

    let fullName2 = "Bucky Barnes";
    console.log("My bestfriend is: " + fullName2); //1. Replaced fullName to fullName2 to match expected output

    const lastLocation = "Arctic Ocean";
    //lastLocation = "Atlantic Ocean"; //1. Comment out new variable declaration for last location since 1st declaration was the expected and it is constant.
    console.log("I was found frozen in: " + lastLocation);



    //Do not modify
    //For exporting to test.js
    //Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
    try{
        module.exports = {
            firstName: typeof firstName !== 'undefined' ? firstName : null,
            lastName: typeof lastName !== 'undefined' ? lastName : null,
            age: typeof age !== 'undefined' ? age : null,
            hobbies: typeof hobbies !== 'undefined' ? hobbies : null,
            workAddress: typeof workAddress !== 'undefined' ? workAddress : null,
            fullName: typeof fullName !== 'undefined' ? fullName : null,
            currentAge: typeof currentAge !== 'undefined' ? currentAge : null,
            friends: typeof friends !== 'undefined' ? friends : null,
            profile: typeof profile !== 'undefined' ? profile : null,
            fullName2: typeof fullName2 !== 'undefined' ? fullName2 : null,
            lastLocation: typeof lastLocation !== 'undefined' ? lastLocation : null
        }
    } catch(err){
    }

