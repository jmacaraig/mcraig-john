console.log("Hi, B297!");



console.log("'Carpe diem. Sieze the day, boys. Make your lives extraordinary.' -Dead Poets Society, 1989");

function printLine(){
	console.log("'Carpe diem. Sieze the day, boys. Make your lives extraordinary.' -Dead Poets Society, 1989");
};

printLine();

//Functions
	//lines/block of code that tell our devices to perform task when called/invoked

//Function declaration

/*
	Syntax:

	function functionName() {
		code block (statement)
	}

*/

function printName() {
	console.log("My name is Cee.")

};

//Function Invocation
printName();


//invocation without declaration results in an error
//invocation can be written first before declaration without an error
declaredFunction();

function declaredFunction(){
	console.log("Hello from declaredFunction!")
}

declaredFunction();

//Function Expression
	//function expression is stored in a variable
	//function expression is an anonymous function assigned to the variable function


//  variableFunction(); //error declared before initialized

//assigning function into variable name or initializing will result to an error if called first before declaring
let variableFunction = function () {
	console.log("Hello from function expression!");
};

variableFunction();


let funcExpression = function funcName() {
	console.log("Hello from the other side!");
};

funcExpression();


//reassigning of declared function
declaredFunction = function(){
	console.log("updated declaredFunction");
};

declaredFunction();

funcExpression = function(){
	console.log("updated funcExpression");
};

funcExpression();


const constantFunc = function(){
	console.log("Initialized with const!")
};

// constant function cannot be re-assigned
// constantFunc = function(){
// 	console.log("Cannot be reassigned!")
// };

// constantFunc();



//Function Scoping
	//Scope - accessibility / visibility of variables
		// 3 Types of JS Variables
				//1. local/block scope
				//2. Global Scope
				//3. Function Scope

// Local / Block Scope a=1
{
	let a = 1;
}

//global Scope a=1
	let a = 1;


//Function Scope a=1
	function sample(){
		let a =1;
	}


{
	let localVar = "Armando Perez"
}

	let globalVar = "Mr. Worldwide";

//	console.log(localVar); // error because it is outside block
	
	console.log(globalVar);


	function showNames(){
		var functionVar = "Joe";
		const functionConst = "John";
		let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
	}

	showNames();


	//Nested Functions

	function myNewFunction(){

		let name = "Jane";

		function nestedFunction(){
		let nestedName = "John"
		console.log(name);
	}
		//console.log(nestedName) - error
		nestedFunction();
}

myNewFunction();



//Global Scoped Variable

let globalName = "Cardo";

function myNewFunction2(){
	let nameInside = "Hillary"
	console.log(globalName);
};

myNewFunction2();
//console.log(nameInside);

function showSampleAlert(){
		alert("Hello, Earthlings! This is from a function")

}

showSampleAlert();


console.log("I will only log in the console when the alert is dismissed");


//let samplePrompt = prompt ("Enter your Name: ");

//console.log("Hi I am "+samplePrompt)


function printWelcomeMessage(){
	let firstName = prompt("Enter your first name: ");
	let lastName = prompt("Enter your last name: ");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to my page!")
}

printWelcomeMessage();


//The Return Statement

function returnFullName(){
	return " Jeffrey" + ' ' + "Smith" + ' ' + "Bezos";
	console.log("This message will not be printed!");
}

//returnFullName();

let fullName = returnFullName();

console.log(fullName);

function returnFullAddress(){
	let fullAddress = {


		street: "44 Maharlika St.",
		city: "Cainta",
		province: "Rizal"

	};

	return fullAddress;
}

let myAddress = returnFullAddress();
console.log(myAddress);


function printPlayerInfo(){

	console.log("Username: " + "dark_magician");
	console.log("Level: " + 95);
	console.log("Job: " + "Mage");

}

let user1 = printPlayerInfo();
console.log(user1); //undefined



function returnSumOf5and10(){
		return 5 + 10;

}

let sumOf5And10 = returnSumOf5and10(); //15
console.log(sumOf5And10);

let total = 100 + returnSumOf5and10(); //115
console.log(total);


function getGuildMembers(){

	return ["Lulu","Tristana","Teemo"];
}

console.log(getGuildMembers());



//Function Naming Conventions

function getCourses(){
	let courses = ["ReactJS 101","ExpressJS 101","MongoDB 101"]

	return courses;
}

let courses = getCourses();
console.log(courses);


//Not descriptive or Generic

// function pikachu(){
// 	let color = "pink";
// 	return name;
// }

function displayCarInfo(){
	console.log("Brand: Toyota");
	console.log("Type: Sedan");
}

displayCarInfo();


//


function getUserInfo(){

		return {

			name: "John Doe",
			age: 25,
			address: "123 Street, Quezon City",
			isMarried: false,
			petName: "Danny"

		}

}

let userInfo = getUserInfo();
console.log(userInfo);