console.log("DOM Manipulation");

console.log(document); //html document
console.log(document.querySelector('#clicker')); //button element

//document refers to the whole webpage
//querySelector is used to select a specific element as long as it is inside html tag using id and class converted to string

//Alternative methods aside from querySelector
	//document.getElementByID
	//document.getElementByClassName
	//document.getElementByTagName


const clicker = document.querySelector('#clicker');

	// clicker.addEventListener('click',function(){
	// 	console.log("The button has been clicked")
	// });

	// clicker.addEventListener('click',()=>{
	// 	console.log("The button has been clicked")
	// });

let counter = 0;

	// clicker.addEventListener('click',()=>{
	// 	console.log("The button has been clicked");
	// 	counter++;
	// 	alert("The button has been clicked "+ counter+ " times.");
	// });

	clicker.addEventListener('click',()=>{
		console.log("The button has been clicked");
		counter++;
		alert(`The button has been clicked ${counter} times.`);
	});

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

// txtFirstName.addEventListener('keyup',function(event){
// 	spanFullName.innerHTML = txtFirstName.value
// });

// txtFirstName.addEventListener('keyup',(event)=>{
// 	spanFullName.innerHTML = txtFirstName.value
// });

// txtLastName.addEventListener('keyup',(event)=>{
// 	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value
// });

const labelFirstname = document.querySelector("#label-txt-first-name");

// labelFirstname.addEventListener('mouseover',function(e){
// 	alert("You hovered the First Name Label!")
// });

labelFirstname.addEventListener('mouseover',(e)=>{
	// alert("You hovered the First Name Label!")
});



const updateFullName = function(e){

	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;

	spanFullName.innerHTML = `${firstName} ${lastName}`

}

txtLastName.addEventListener('keyup', updateFullName);
txtFirstName.addEventListener('keyup', updateFullName);


