import {useState, useEffect} from 'react'; 
import {Link} from 'react-router-dom';
import {CardGroup} from 'react-bootstrap'; 

import PreviewCourses from './PreviewCourses.js';


//CHATGPT

// export default function FeaturedCourses() {
//   // Add states
//   const [previews, setPreviews] = useState([]);

//   useEffect(() => {
//     fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
//       .then((res) => res.json())
//       .then((data) => {
//         console.log(data);
//         const numbers = [];
//         const featured = [];

//         const generateRandomNums = () => {
//           if (numbers.length === data.length || data.length === 0) {
//             // All 5 unique numbers have been generated or there's no data
//             return;
//           }

//           let randomNum;
//           do {
//             randomNum = Math.floor(Math.random() * data.length);
//           } while (numbers.indexOf(randomNum) !== -1);

//           numbers.push(randomNum);

//           // Continue generating numbers until you have 5 unique ones
//           generateRandomNums();
//         };

//         generateRandomNums(); // Generate 5 unique random numbers

//         for (let i = 0; i < 5; i++) {
//           const randomIndex = numbers[i];

//           // Check if the randomIndex is within valid range
//           if (randomIndex >= 0 && randomIndex < data.length) {
//             featured.push(
//               <PreviewCourses data={data[randomIndex]} key={data[randomIndex]._id} breakPoint={2} />
//             );
//           }
//         }

//         setPreviews(featured);
//       });
//   }, [])


//ZUITT

export default function FeaturedCourses(){

	//add states
	const [ previews, setPreviews] = useState([]);

	useEffect(()=>{

		fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
		.then(res=>res.json())
		.then(data=>{
			console.log(data);
			const numbers = [];
			const featured = [];

		const generateRandomNums = () => {
			let randomNum = Math.floor(Math.random()*data.length)

			if (numbers.indexOf(randomNum) === -1){
				numbers.push(randomNum);
			} else {
				generateRandomNums()
			}

		}


		for(let i=0; i<data.length; i++){

			generateRandomNums()

			featured.push(<PreviewCourses data={data[numbers[i]]} key={data[numbers[i]]._id} breakPoint={2} />)

		}

		setPreviews(featured)

		})

	},[])

	return(
		<>
			<h2></h2>
			<CardGroup className="justify-content-center">
				
				{previews}
			</CardGroup>
		</>

	)

	}