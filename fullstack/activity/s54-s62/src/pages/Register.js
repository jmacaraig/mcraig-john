import {Form, Button} from 'react-bootstrap';
import {useState, useEffect} from 'react';


export default function Register(){

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	const [isActive, setIsActive] = useState(false);

	console.log(firstName);
	console.log(lastName);
	console.log(email);
	console.log(mobileNo);
	console.log(password);
	console.log(confirmPassword);


	//fetch

	function registerUser(e){

		e.preventDefault();

		fetch('http://localhost:4000/users/register',{
			method: 'POST',
			headers:{
				"Content-Type":"application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password
			})

		})
		.then(res=>res.json())
		.then(data=>{

			console.log(data)
			if (data){

				setFirstName("");
				setLastName("");
				setEmail("");
				setMobileNo("");
				setPassword("");
				setConfirmPassword("");

				alert("Thank you for registering!")

			} else {
				alert("Please try again.")
			}

		})

	}


	useEffect(()=>{

		if(firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !== "" && confirmPassword !== "" && (password === confirmPassword) && (mobileNo.length === 11)){

			setIsActive(true)

		}else{

			setIsActive(false)

		}

	},[firstName,lastName,email,mobileNo,password,confirmPassword])


	return (

		<Form onSubmit={(e)=>registerUser(e)}>
			<h1 className = "my-5 text-center">Register</h1>

			<Form.Group>
				<Form.Label>First Name:</Form.Label>
				<Form.Control type="text" placeholder="Enter First Name" required value={firstName} onChange={e=>{setFirstName(e.target.value)}}/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Last Name:</Form.Label>
				<Form.Control type="text" placeholder="Enter Last Name" required value={lastName} onChange={e=>{setLastName(e.target.value)}}/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Email:</Form.Label>
				<Form.Control type="email" placeholder="Enter Email" required value={email} onChange={e=>{setEmail(e.target.value)}}/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Mobile No:</Form.Label>
				<Form.Control type="text" placeholder="Enter Mobile 11 digit No." required value={mobileNo} onChange={e=>{setMobileNo(e.target.value)}}/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password:</Form.Label>
				<Form.Control type="password" placeholder="Enter password" required value={password} onChange={e=>{setPassword(e.target.value)}}/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Confirm Password:</Form.Label>
				<Form.Control type="password" placeholder="Confirm password" required value={confirmPassword} onChange={e=>{setConfirmPassword(e.target.value)}}/>
			</Form.Group>

			{/*<Button variant="primary" type="submit">Submit</Button>*/}

			{
				isActive ?
				<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
				:
				<Button variant="danger" type="submit" id="submitBtn">Submit</Button>
			}

		</Form>
		

	)

}