console.log("Hello, B297");

//[if, else if, and else statements]

let numG = -1;

//if statement - executes statement if the if condition is true or met


if (numG < 0){
  console.log("Hello! The condition in the if statement is true")
}

// if (numG > 0){
//   console.log("Hello! The condition in the if statement is true")
// }

let numH = 1;

//else if - executes statement if the if condition is false and else if is true

if (numG > 0) {
  console.log("Hello")
}

else if (numH > 0){
  console.log ("This will log if the else if condition is true and the if condition is false")
}


//else statement - executes statement if the if condition is false and else if is false

if (numG > 0) {
  console.log("I'm enchanted to meet you")
}

else if (numH = 0){
  console.log ("It's me, Hi")
}
else {
  console.log("Hello from the other side!")
}


//[if, else if, and else statements with functions]

let message = "No message!";
console.log(message);

    function determineTyphoonIntensity(windSpeed) {
      
        if (windSpeed < 30){
            return "Not a typhoon yet!"
        }
        else if (windSpeed <= 61){
          return "Tropical Depression detected!"
        }
        else if (windSpeed >= 62 && windSpeed <= 88){
          return "Tropical Storm detected!"
        }
        else if (windSpeed >= 89 && windSpeed <= 117){
          return "Severe Tropical Storm detected!"
        }
        else {
          return "Typhoon Detected!"
        }

    }

    message = determineTyphoonIntensity(65);
    console.log(message);

    //console.warn() - print warnings in console that help devs debug certain output within our code


    if (message == "Tropical Storm detected!"){
      console.warn(message)
    }

//truthy and falsy

    // truthy is value true in boolean
    // falsy is value false in boolean
      // false, 0 , -0 , "" empty string , null , undefined, NaN


//Truthy Examples

    if (true){
      console.log("This is truthy!")
    }

    if (1){
      console.log("This is truthy!")
    }

    if([]){
      console.log ("This is truthy!")
    }

//Falsy Examples

    if (false){
      console.log("This will not log in the console!")
    }

    if (0){
      console.log("This will not log in the console!")
    }

    if(undefined){
      console.log("This will not log in the console!")
    }    
    if ("B297"){
      console.log("This will log in the console.")
    }

//conditional ternary operator

    //Takes in three operands:
      //condition
      //expression to execute if the condition is truthy
      //expression to execute if the condition is falsy

    //Syntax:
      //(condition) ? ifTru : ifFalse

//single statement execution

let ternaryResult = (1 < 18) ? true : false;
console.log ("Result of ternary operator: " + ternaryResult);


//multiple statement execution

let name;

function isOfLegalAge(){
  name = 'John';
  return "You are of the legal age limit!"
}

function isUnderAge() {
  name = "Jane";
  return "You are under the age limit!"
}

// let age = parseInt(prompt("What is your age?"))
// console.log(age);

// let legalAge = (age>18) ? isOfLegalAge() : isUnderAge();
// console.log("Result of ternary operator in functions: " + legalAge + ", " + name);

//[switch statement]

  //can be used as an alternative to an if, else if, and else statement where the data to be used in the condition is of an EXPECTED input


/*

  Syntax
    switch (expression){
      
      case 
    }

*/

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

    switch (day) {

        case 'monday':
            console.log("The color of the day is blue");
            break;
        case 'tuesday':
            console.log("The color of the day is yellow");
            break;
        case 'wendesday':
            console.log("The color of the day is red");
            break;
        case 'thursday':
            console.log("The color of the day is green");
            break;
        case 'friday':
            console.log("The color of the day is orange");
            break;
        case 'saturday':
            console.log("The color of the day is violet");
            break;
        case 'sunday':
            console.log("The color of the day is indigo");
            break;

        default:
          console.log("Please input a valid day");
          break;

    }

//[try-catch-finally statement]
    //try catch statements are commonly used for error handling

  function showIntensityAlert(windSpeed){

    try {
      //attempt to execute a code
      alerat(determineTyphoonIntensity(windSpeed));
    }

    catch (error) {
      //err.message is used to access the information relating to the error object
      console.log(typeof error);
      console.warn(error.message)

    }

    finally {
      alert("Intensity updates will show new alert")
    }

  }

  showIntensityAlert(56);

  console.log("Hi!")





 





