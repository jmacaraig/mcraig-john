import {useState, useEffect, useContext} from 'react';
import {useParams, useNavigate, Link} from 'react-router-dom';
import {Container, Card, Button, Row, Col} from 'react-bootstrap';
import Swal from 'sweetalert2';

import UserContext from "../UserContext";


export default function CourseView(){

	const { user } = useContext(UserContext);

	const navigate = useNavigate();


	const {courseId} = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const enroll = (courseId) =>{

		fetch(`${ process.env.REACT_APP_API_URL}/users/enroll`,{
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},

			body: JSON.stringify({
				courseId: courseId
			})

		})
		.then(res => res.json())
		.then(data => {
			
			console.log(data);

			if (data.message = 'Enrolled Successfully.'){
				Swal.fire({
					title: "Successfully Enrolled!",
					icon: "Success",
					text: "You have successfully enrolled for the course."
				})
			} else {

				Swal.fire({
					title: "Something went wrong.",
					icon: "Error",
					text: "Please try again."
				})

			}

		})

	}

	useEffect(() =>{

		console.log(courseId);

		fetch(`${ process.env.REACT_APP_API_URL}/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		})

	}, [courseId]);

	return (

		<Container className = "mt-5">

			<Row>
				<Col lg={{span: 6, offset:3}}>
					<Card.Body className="text-center">
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Descirption:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>{price}</Card.Text>
						<Card.Subtitle>Class Schedule:</Card.Subtitle>
						<Card.Text>8:00 AM - 5:00 PM</Card.Text>
						{user.id !==null?

						<Button variant="primary" block onClick={()=> enroll(courseId)}>Enroll</Button>
						:
						<Link className="btn btn-danger btn-block">Log in to Enroll</Link>

						}
						
					</Card.Body>
				</Col>
			</Row>

		</Container>

	)

}
