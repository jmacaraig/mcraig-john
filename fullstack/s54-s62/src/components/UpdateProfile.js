import { Button, Modal, Form } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import Swal from 'sweetalert2';



export default function EditProfile({fetchData}) {

		const [firstName, setFirstName] = useState('');
		const [lastName, setLastName] = useState('');
		const [mobileNo, setMobileNo] = useState('');

		const [showEdit, setShowEdit] = useState(false);

		const openEdit = ()=> {

			fetch(`${ process.env.REACT_APP_API_URL}/users/details`)
			.then(res=>res.json())
			.then(data=>{

				setFirstName(data.firstName);
				setLastName(data.lastName);
				setMobileNo(data.mobileNo);

			})

			setShowEdit(true);
		}

		const closeEdit = () => {

				
				setShowEdit(false);
				setFirstName('');
				setLastName('');
				setMobileNo('');			

		}

		const editProfile = ()=>{



			fetch(`${ process.env.REACT_APP_API_URL}/users/profile`,{

				method:'PUT',
				headers:{
					'Content-Type':'application/json',
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					mobileNo: mobileNo

				})


			})
			.then(res=>res.json())
			.then(data=>{
				console.log(data);

				if (data === true){
					Swal.fire({
						title: "Success.",
						icon: "success",
						text: "Profile Successfuly Updated"
					})

				closeEdit();
				fetchData();

				} else {
					Swal.fire({
						title: "Error.",
						icon: "error",
						text: "Please try again."
					})
				closeEdit();
				fetchData();
				}

			})


		}

	return (

		<>
			<Button className="m-5" variant="primary" size="m" onClick={()=>openEdit()}>Edit Profile</Button>
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e=>editProfile()}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Profile</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form.Group controlId="firstName">
							<Form.Label>First Name</Form.Label>
							<Form.Control type="text"
							value={firstName}
							onChange={e=>setFirstName(e.target.value)}
							required/>
						</Form.Group>

						<Form.Group controlId="lastName">
							<Form.Label>Last Name</Form.Label>
							<Form.Control type="text"
							value={lastName}
							onChange={e=>setLastName(e.target.value)}
							required/>
						</Form.Group>

						<Form.Group controlId="mobileNo">
							<Form.Label>Mobile Number</Form.Label>
							<Form.Control type="text"
							value={mobileNo}
							onChange={e=>setMobileNo(e.target.value)}
							required/>
						</Form.Group>

					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

		</>


	)

}