import Banner from '../components/Banner.js';
import Highlights from '../components/Highlights.js';
import Course from '../components/Course.js';
import Container from 'react-bootstrap/Container';

export default function Home () {

	return (
	<>  
      <Banner />
      <Highlights />
      <Course />
    </>

	)

}