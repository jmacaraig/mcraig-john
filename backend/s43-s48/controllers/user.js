//controller 

const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqbody) => {

	return User.find({email: reqbody.email})

	.then(result => {

		if (result.length > 0){
			return true;
		}
		else {
			return false;
		}

	})

}

module.exports.registerUser = (reqbody) => {

	let newUser = new User ({

		firstName: reqbody.firstName,
		lastName: reqbody.lastName,
		email: reqbody.email,
		password:bcrypt.hashSync(reqbody.password,10),
		mobileNo:reqbody.mobileNo	
	})

	return newUser.save().then((user,error)=>{

		if (error){
			return true;
		}
		else {
			return false;
		}

	
	})

	.catch(err=>err)

}

module.exports.loginUser = (req,res)=>{

	console.log(req.body)
	return User.findOne({email: req.body.email})
	.then(result=>{

		if (result===null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password)
		
		if (isPasswordCorrect){

			return res.send({access: auth.createAccessToken(result)})

		}else {
			return res.send(false);
		}
	}
	})
	.catch(err => res.send(err))
} 


module.exports.getProfile = (req,res)=>{
	
	console.log(req.body)
	return User.findById(req.user.id)
	.then(result=>{

		result.password = "";
		return res.send(result);
	
	})
	

} 


// module.exports.getProfile = (user)=>{

// 	return User.findById({_id: user.id})
// 	.then(result=>{

// 		result.password = "";
// 		return result
	
// 	})
	
// } 


module.exports.enroll = async (req,res) => {

	// console.log(req.user.id);
	console.log(req.body.courseId);

	if (req.user.isAdmin){
		return res.send("Action Forbidden");
	}

	let isUserUpdated = await User.findById(req.user.id).then(user =>{

		let newEnrollment = {

			courseId: req.body.courseId

		}

		user.enrollments.push(newEnrollment);


		console.log(user.enrollments);
		return user.save().then(user=> true).catch(err => err.message);
	
	})

	if (isUserUpdated !== true){

		return res.send({message: isUserUpdated});

	}

	

	let isCourseUpdated = await Course.findById(req.body.courseId).then(course =>{

		let newEnrollee = {

			userId: req.user.id

		}



		course.enrollees.push(newEnrollee);

		return course.save().then(course => true).catch(err => err.message);


})
	if (isCourseUpdated !== true){

		return res.send({message: isCourseUpdated});

	}

	if (isUserUpdated && isCourseUpdated){

		return res.send({message: "Enrolled Successfully."})

	}

}

module.exports.getEnrollments = (req,res)=>{
	
	console.log(req.user.id);

	return User.findById(req.user.id).then(user => /*res.send(user.enrollments)).catch(err=>res.send(err))*/{

	let enrolledCourses =  user.enrollments


	console.log(enrolledCourses);
	res.send(enrolledCourses);

	})
}

module.exports.resetPassword = async (req, res) => {
  try {
    const { newPassword } = req.body;
    const { id } = req.user; // Extracting user ID from the authorization header

    // Hashing the new password
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    // Updating the user's password in the database
    await User.findByIdAndUpdate(id, { password: hashedPassword });

    // Sending a success response
    res.status(200).json({ message: 'Password reset successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};
 

module.exports.updateProfile = async (req,res)=>{
  try {
    // Get the user ID from the authenticated token
    const userId = req.user.id;

    // Retrieve the updated profile information from the request body
    const { firstName, lastName, mobileNo } = req.body;

    // Update the user's profile in the database
    const updatedUser = await User.findByIdAndUpdate(
      userId,
      { firstName, lastName, mobileNo },
      { new: true }
    );

    res.json(updatedUser);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Failed to update profile' });
  }
}



 module.exports.updateAsAdmin = async (req, res) => {
    const userId = req.body.userId;

    try {
      // Check if the user making the request is an admin
      if (!req.user.isAdmin) {
        return res.status(403).json({ message: "You don't have permission to perform this action." });
      }

      // Update the user's isAdmin status
      const updatedUser = await User.findByIdAndUpdate(
        userId,
        { isAdmin: true },
        { new: true }
      );

      if (!updatedUser) {
        return res.status(404).json({ message: 'User not found.' });
      }

      return res.json({ message: 'User has been updated as an admin.' });
    } catch (error) {
      console.error(error);
      return res.status(500).json({ message: 'An error occurred while updating the user.' });
    }
  };


module.exports.updateEnrollmentStatus = async (req, res) => {
  const userId = req.body.userId;
  const courseId = req.body.courseId;
  const enrollmentStatus = req.body.enrollmentStatus.toLowerCase(); // Convert status to lowercase

  try {
    // Check if the user making the request is an admin 
    if (!req.user.isAdmin) {
      return res.status(403).json({ message: "You don't have permission to perform this action." });
    }

    // Find the user and the course
    const user = await User.findById(userId);
    const course = await Course.findById(courseId);

    if (!user || !course) {
      return res.status(404).json({ message: 'User or course not found.' });
    }

    // Debug: Check user and course data
    console.log('User:', user);
    console.log('Course:', course);

    // Find the specific enrollment entry for the course and update status
    const enrollmentToUpdate = user.enrollments.find(enrollment => enrollment.courseId.toString() === courseId);
    if (!enrollmentToUpdate) {
      return res.status(404).json({ message: 'Enrollment not found.' });
    }

    enrollmentToUpdate.status = enrollmentStatus; // Update the enrollment status
    await user.save(); // Save the changes to the user object

    res.json({ message: 'Enrollment status updated successfully.' });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: 'An error occurred while updating enrollment status.' });
  }
};
